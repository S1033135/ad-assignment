def generate_country_dropdown():
    countries = ["United America", "England", "France", "Germany", "Canada", "Other"]  # List of countries

    print("Select your country:")
    for index, country in enumerate(countries, start=1):
        print(f"{index}. {country}")

    selected_index = int(input("Enter the number corresponding to your country: "))
    selected_country = countries[selected_index - 1]

    return selected_country

def current_weather_dropdown():
    weathers = ["sunny", "cloudy", "rainy", "snowy", "windy", "other"]  # List of weathers

    print("Select your current weather:")
    for index, weather in enumerate(weathers, start=1):
        print(f"{index}. {weather}")

    selected_index = int(input("Enter the number corresponding to your current weather: "))
    selected_current_weather = weathers[selected_index - 1]

    return selected_current_weather

from PIL import Image
import os

def open_resize_image(original_image_file_path, new_size):
    image = Image.open(original_image_file_path)            # Open an image file
    filename, extension = os.path.splitext(os.path.basename(original_image_file_path))      # Get the filename and extension from the original image path
    resized_filename = f"{filename}_resized{extension}"     # Generate a new filename for the resized image
    resized_image = image.resize(new_size)                  # Resize the image
    resized_image.save(resized_filename)                    # Save the resized image with the new filename
    resized_image.show()                                    # Show the resized image

class ClothingSuggestionApp:                                # Initialize the class and set up initial value
    def __init__(self):
        self.gender = None
        self.age = None
        self.country = None
        self.weather = None
        self.image = None

    def get_user_input(self):                               # User input for gender, age, country and weather
        self.gender = input("Enter your gender (Male/Female): ")
        self.age = int(input("Enter your age: "))
        self.country = generate_country_dropdown()
        self.weather = current_weather_dropdown()

    def suggest_clothes(self):                              # Suggest cloth base on user input
        suggestion = ""

        if self.gender.lower() == "male":
            if self.age > 18:
                suggestion += "man "
            else:
                suggestion += "boys "
        elif self.gender.lower() == "female":
            if self.age > 18:
                suggestion += "woman "
            else:
                suggestion += "girls "
        else:
            suggestion += "a unisex "

        if self.weather.lower() == "sunny":
            suggestion += "shorts and t-shirt"
            original_image_file_path = "sunny.jpg"
            new_size = (400, 600)
            self.image = open_resize_image(original_image_file_path, new_size)
        elif self.weather.lower() == "cloudy":
            suggestion += "denim jacket and pants"
            original_image_file_path = "cloudy.jpg"
            new_size = (400, 600)
            self.image=open_resize_image(original_image_file_path, new_size)
        elif self.weather.lower() == "rainy":
            suggestion += "raincoat and umbrella"
            original_image_file_path = "rainy.jpg"
            new_size = (400, 600)
            self.image = open_resize_image(original_image_file_path, new_size)
        elif self.weather.lower() == "snowy":
            suggestion += "waterproof insulated jackets and pants"
            original_image_file_path = "snowy.jpg"
            new_size = (400, 600)
            self.image = open_resize_image(original_image_file_path, new_size)
        elif self.weather.lower() == "windy":
            suggestion += "windproof hoodie and jean pants"
            original_image_file_path = "windy.jpg"
            new_size = (400, 600)
            self.image = open_resize_image(original_image_file_path, new_size)
        else:
            suggestion += "business casual"
            original_image_file_path = "business_casual.jpg"
            new_size = (400, 600)
            self.image = open_resize_image(original_image_file_path, new_size)
        return suggestion

    def display_suggestion(self, suggestion):                   # Display the suggested cloth
        print(f"Based on information you provided, we suggest '{suggestion}', considering your preferences.")

if __name__ == "__main__":
    app = ClothingSuggestionApp()
    app.get_user_input()
    suggested_clothes = app.suggest_clothes()
    app.display_suggestion(suggested_clothes)